import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  creator_one : string = '';
  creator_two : string = '';

  constructor() {
    this.creator_one = 'Carlos';
    this.creator_two = 'Simón';
  }

  ngOnInit(): void {
  }

}
