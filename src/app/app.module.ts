import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeContainerComponent } from './pages/home-page/components/home-container/home-container.component';
import { PlanetsComponent } from './pages/home-page/components/home-container/planets/planets.component';
import { CharactersComponent } from './pages/home-page/components/home-container/characters/characters.component';
import { SeasonsComponent } from './pages/home-page/components/home-container/seasons/seasons.component';
import { HeaderComponent } from './pages/home-page/components/home-container/header/header.component';
import { FooterComponent } from './pages/home-page/components/home-container/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeContainerComponent,
    PlanetsComponent,
    CharactersComponent,
    SeasonsComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
